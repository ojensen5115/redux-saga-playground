import * as types from '../types';

const initialApplicationState: types.AppState = {
  counter: 0
}

export default initialApplicationState;
