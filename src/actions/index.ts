import onIncrement from './onIncrement';
import onDecrement from './onDecrement';
import onIncrementAsync from './onIncrementAsync';

export default {
  onIncrement,
  onDecrement,
  onIncrementAsync
}
